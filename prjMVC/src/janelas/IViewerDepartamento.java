package janelas;

public interface IViewerDepartamento {

	/**
	 * Determina os valores para os campos da janela
	 * @param sigla
	 * @param nome
	 */
	public abstract void atualizarCampos(String sigla, String nome);

	public void setVisible(boolean flag);
}