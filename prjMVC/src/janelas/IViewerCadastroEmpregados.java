package janelas;

import controle.ITabelavel;

public interface IViewerCadastroEmpregados {

	/**
	 * Remove todas as linhas da tabela
	 */
	public abstract void limpar();

	/**
	 * Inclui uma linha na tabela
	 * @param linha
	 */
	public abstract void incluirLinha(ITabelavel objeto);

	public abstract void executarIncluir();

	public abstract void executarExcluir();

	public abstract void executarAlterar();

	public abstract void executarTerminar();
	
	public abstract void setVisible(boolean flag);
}