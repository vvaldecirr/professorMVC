package dados;

import java.io.Serializable;
import controle.ITabelavel;

public class Empregado implements Serializable, ITabelavel, Comparable<Empregado> {
	//
	// CONSTANTES
	//
	public static final int TAMANHO_CPF = 11;
	public static final int TAMANHO_NOME = 40;
	
	//
	// ATRIBUTOS
	//
	private String cpf;
	private String nome;
	private Departamento depto;

	//
	// M�TODOS
	//
	public Empregado(String cpf, String nome, Departamento d) throws DadosException {
		super();
		this.setCpf(cpf);
		this.setNome(nome);
		this.setDepto(d);
	}

	public String getCpf() {
		return cpf;
	}

	@RegraDeDominio
	public static void validarCpf(String cpf) throws DadosException {
		if(cpf == null || cpf.length() == 0) 
			throw new DadosException("O CPF n�o pode ser nulo!");
		for(int i = 0; i < cpf.length(); i++)
			if(!Character.isDigit(cpf.charAt(i)))
				throw new DadosException("O CPF s� deve digitos!");		
		if(cpf.length() != TAMANHO_CPF)
			throw new DadosException("O CPF deve ter " + TAMANHO_CPF + " digitos!");		
	}

	public void setCpf(String cpf) throws DadosException {
		validarCpf(cpf);
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	@RegraDeDominio
	public static void validarNome(String nome) throws DadosException {
		if(nome == null || nome.length() == 0) 
			throw new DadosException("O Nome n�o pode ser nulo!");
		if(nome.length() > 40)
			throw new DadosException("O Nome n�o deve exceder a " + TAMANHO_NOME + " caracteres!");		
	}

	public void setNome(String nome) throws DadosException {
		validarNome(nome);
		this.nome = nome;
	}

	public Departamento getDepto() {
		return depto;
	}

	public void setDepto(Departamento depto) {
		if(this.depto == depto)
			return;
		if(depto == null) {
			Departamento antigo = this.depto; 
			this.depto = null;
			antigo.removerEmpregado(this);
		}
		else {
			if(this.depto != null)
				this.depto.removerEmpregado(this);
			this.depto = depto;
			this.depto.adicionarEmpregado(this);							
		}
			
	}

	/**
	 * Implementa��o do m�todo toString que retorna uma String que descreve o
	 * objeto Empregado
	 */
	public String toString() {
		return this.cpf + "-" + this.nome;
	}

	/**
	 * Retorna um array de Objects com os estados dos atributos dos objetos
	 * 
	 * @return
	 */
	public Object[] getData() {
		return new Object[] { this.cpf, this.nome, this.depto.getNome() };
	}

	/**
	 * M�todo utilizado para colocar os empregados em ordem
	 */
	public int compareTo(Empregado d) {
		return this.nome.compareTo(d.nome);
	}
}