package dados;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import controle.ITabelavel;

/**
 * Implementa a classe Departamento que tem o "implements Serializable"
 * para realizar o processo de serializa��o e o "implements Tabelavel"
 * para informar que os objetos poder�o ser exibidos em uma 
 * tabela de interface
 * @author Alessandro Cerqueira
 *
 */
public class Departamento implements Serializable, ITabelavel, Comparable<Departamento> {	
	//
	// CONSTANTES
	//
	public static final int TAMANHO_SIGLA = 2;
	public static final int TAMANHO_NOME = 40;
	
	//
	// ATRIBUTOS
	//
	private String sigla;
	private String nome;
	
	private Set<Empregado> listaEmpregados;
	
	//
	// M�TODOS
	//
	public Departamento(String sigla, String nome) throws DadosException {
		super();
		this.setSigla(sigla);
		this.setNome(nome);
		this.listaEmpregados = new TreeSet<Empregado>();
	}

	public String getSigla() {
		return sigla;
	}

	@RegraDeDominio
	public static void validarSigla(String sigla) throws DadosException {
		if(sigla == null || sigla.length() == 0) 
			throw new DadosException("A Sigla n�o pode ser nula!");
		if(sigla.length() != TAMANHO_SIGLA)
			throw new DadosException("A Sigla deve apresentar dois caracteres!");		
	}

	public void setSigla(String sigla) throws DadosException {
		validarSigla(sigla);
		this.sigla = sigla;
	}
	
	public String getNome() {
		return nome;
	}
	
	@RegraDeDominio
	public static void validarNome(String nome) throws DadosException {
		if(nome == null || nome.length() == 0) 
			throw new DadosException("O Nome n�o pode ser nulo!");
		if(nome.length() > 40)
			throw new DadosException("O Nome n�o deve exceder a " + TAMANHO_NOME + " caracteres!");		
	}

	public void setNome(String nome) throws DadosException {
		validarNome(nome);
		this.nome = nome;
	}
	
	public boolean adicionarEmpregado(Empregado novo) {
		if(this.listaEmpregados.contains(novo))
			return false;
		this.listaEmpregados.add(novo);
		novo.setDepto(this);
		return true;
		
	}

	public boolean removerEmpregado(Empregado ex) {
		if(!this.listaEmpregados.contains(ex))
			return false;
		this.listaEmpregados.remove(ex);
		ex.setDepto(null);
		return true;
		
	}
	/** 
	 * Implementa��o do m�todo toString que retorna uma String
	 * que descreve o objeto Departamento
	 */
	public String toString() {
		return this.sigla + "-" + this.nome;
	}

	/**
	 * Retorna um array de Objects com os estados dos atributos 
	 * dos objetos
	 * @return
	 */
	public Object[] getData() {
		return new Object[]{this.sigla, this.nome, this.listaEmpregados.size()};
	}
	
	/**
	 * M�todo utilizado para colocar os departamentos em ordem 
	 */
	public int compareTo(Departamento d) {
		return this.nome.compareTo(d.nome);
	}
}
