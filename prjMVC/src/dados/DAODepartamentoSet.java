package dados;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Set;
import java.util.TreeSet;

public class DAODepartamentoSet implements IDAO<Departamento>, IDAOSerializavel {
	//
	// ATRIBUTOS
	//
	/**
	 * Refer�ncia para a �nica inst�ncia da classe que dever� existir
	 */
	private static IDAO<Departamento> 	singleton;
	/**
	 * Refer�ncia para o Set que apontar� para todos os objetos 
	 * guardados pelo DAO
	 */
	private Set<Departamento> 			listaObjs;
	
	//
	// M�TODOS
	//
	/**
	 * Construtor privado do DAO
	 */
	private DAODepartamentoSet() {
		// Aloco mem�ria para o array
		this.listaObjs = new TreeSet<Departamento>();
	}
	
	/**
	 * M�todo para retornar a �nica inst�ncia existente do DAO
	 * @return
	 */
	public static IDAO<Departamento> getSingleton() {
		if(DAODepartamentoSet.singleton == null)
			DAODepartamentoSet.singleton = new DAODepartamentoSet();
		return DAODepartamentoSet.singleton;
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAODepartamento#salvar(dados.Departamento)
	 */
	@Override
	public boolean salvar(Departamento novo){
		return this.listaObjs.add(novo);
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAODepartamento#remover(dados.Departamento)
	 */
	@Override
	public boolean remover(Departamento obj){
		return this.listaObjs.remove(obj);
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAODepartamento#atualizar(dados.Departamento)
	 */
	@Override
	public boolean atualizar(Departamento obj){
		return true;
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAODepartamento#recuperar(int)
	 */
	@Override
	public Departamento recuperar(int pos){
		int i = 0;
		for(Departamento d : this.listaObjs)
			if(i++ == pos)
				return d;
		return null;
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAO#recuperarPelaSigla(java.lang.String)
	 */
	@Override
	public Departamento recuperarPelaChave(Object sigla){
		for(Departamento d : this.listaObjs)
			if(d.getSigla().equals(sigla))
				return d;
		return null;
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAODepartamento#getNumObjs()
	 */
	@Override
	public int getNumObjs(){
		return this.listaObjs.size();
	}
	
	/* (non-Javadoc)
	 * @see dados.IDAODepartamento#getListaObjs()
	 */
	@Override
	public Departamento[] getListaObjs() {
		return (Departamento[])this.listaObjs.toArray();
	}

	/* (non-Javadoc)
	 * @see dados.ISerializarDAO#recuperarObjetos(java.io.ObjectInputStream)
	 */
	@Override
	public void recuperarObjetos(ObjectInputStream ois) 
			throws IOException, ClassNotFoundException {
		// Recupera o array de objetos
		this.listaObjs = (Set<Departamento>)ois.readObject();
	}

	/* (non-Javadoc)
	 * @see dados.ISerializarDAO#salvarObjetos(java.io.ObjectOutputStream)
	 */
	@Override
	public void salvarObjetos(ObjectOutputStream oos) 
			throws IOException {
		// Salva o array de objetos
		oos.writeObject(this.listaObjs);
	}
}
