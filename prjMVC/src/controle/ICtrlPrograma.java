package controle;

public interface ICtrlPrograma {

	/**
	 * Procedimentos executados no in�cio do programa
	 */
	public abstract void iniciar();

	/**
	 * Procedimentos executados no final do programa
	 */
	public abstract void terminar();

	/**
	 * Procedimentos executados pelo controlador do programa
	 * para iniciar o caso de uso Manter Departamentos.
	 */
	public abstract boolean iniciarCasoDeUsoManterDepartamentos();

	/**
	 * Procedimentos executados pelo controlador do programa
	 * para finalizar o caso de uso Manter Departamentos.
	 */
	public abstract boolean terminarCasoDeUsoManterDepartamentos();

	/**
	 * Procedimentos executados pelo controlador do programa
	 * para iniciar o caso de uso Manter Empregados.
	 */
	public abstract boolean iniciarCasoDeUsoManterEmpregados();

	/**
	 * Procedimentos executados pelo controlador do programa
	 * para finalizar o caso de uso Manter Empregados.
	 */
	public abstract boolean terminarCasoDeUsoManterEmpregados();

}